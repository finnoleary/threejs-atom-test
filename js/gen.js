
var gen = {
  create_cube: function (x, y, z, col)
  {
    var geometry = new THREE.BoxGeometry(1, 1, 1);
    var material = new THREE.MeshBasicMaterial( { color: col} );
    var cube = new THREE.Mesh( geometry, material );
    cube.position.z = z;
    cube.position.y = y;
    cube.position.x = x;
    scene.add(cube);
    return cube
  },

  load_map: function (array)
  {
    for (var j = 0; j < array.length; j++) {
      for (var i = 0; i < array[j].length; i++) {
        if (array[j][i] == 1) {
          array[j][i] = gen.create_cube(i, 1, j, 0xFF0000);
        }
      }
    }
    return array;
  }
}
