var width = 1022.9;
var height = 764.9;
var fov = 90;

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(fov, width / height, 0.1, 1000);
var renderer = new THREE.WebGLRenderer();
renderer.setSize(width, height);

camera.rotation.order = 'YXZ';
document.body.appendChild(renderer.domElement);
